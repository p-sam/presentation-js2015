[![Website](https://img.shields.io/website-up-down-green-red/https/presentation-js2015.netlify.com.svg?label=site)](https://presentation-js2015.netlify.com) [![License](https://img.shields.io/badge/license-MIT-lightgrey.svg)](LICENSE) ![node](https://img.shields.io/badge/node-%3E%3D0.10.0-green.svg) ![vu à la tv](https://img.shields.io/badge/vu-%C3%A0%20la%20tv-blue.svg) 

# presentation-js2015
Présentation de certains changements apportés par ES6 et ES7

Dispo sur [https://presentation-js2015.netlify.com](https://presentation-js2015.netlify.com)

## Usage

```sh
npm install
npm start # port 8080
```

**Note**: Appuyer sur `F2` sur le slide permet d'afficher les notes

## Basé sur

* [bespoke.js](https://github.com/bespokejs/bespoke) - Le framework pour les slides
* [primjs](http://prismjs.com/) - Coloration syntaxique
* [twig.js](https://github.com/twigjs/twig.js) - Templating
* [Gulp + plugins](https://gulpjs.com/) - Compiler les assets