'use strict';

var pkg = require('./package.json');
var gulp = require('gulp');
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var connect = require('gulp-connect');
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var sass = require('gulp-sass');
var twig = require('gulp-twig');
var del = require('del');
var through = require('through');
var packageImporter = require('node-sass-package-importer')();
var path = require('path');

var twigExtend = require('./twig-extend.js');

var isDist = process.env.NODE_ENV === 'production';

gulp.task('js', ['clean:js'], function() {
	return gulp.src('src/scripts/main.js')
		.pipe(isDist ? through() : plumber())
		.pipe(browserify({ debug: !isDist }))
		.pipe(isDist ? uglify() : through())
		.pipe(rename('build.js'))
		.pipe(gulp.dest('dist/build'))
		.pipe(connect.reload());
});

gulp.task('html', ['clean:html'], function() {
	return gulp.src('src/index.html.twig')
		.pipe(isDist ? through() : plumber())
		.pipe(twig({extend: twigExtend}))
		.pipe(rename('index.html'))
		.pipe(gulp.dest('dist'))
		.pipe(connect.reload());
});

gulp.task('css', ['clean:css'], function() {
	return gulp.src('src/styles/main.scss')
		.pipe(isDist ? through() : plumber())
		.pipe(sass({
			importer: packageImporter
		}))
		.pipe(autoprefixer('last 2 versions', { map: false }))
		.pipe(isDist ? csso() : through())
		.pipe(rename('build.css'))
		.pipe(gulp.dest('dist/build'))
		.pipe(connect.reload());
});

gulp.task('images', ['clean:images'], function() {
	return gulp.src('src/images/**/*')
		.pipe(gulp.dest('dist/images'))
		.pipe(connect.reload());
});

gulp.task('clean', function(done) {
	del('dist', done);
});

gulp.task('clean:html', function(done) {
	del('dist/index.html', done);
});

gulp.task('clean:js', function(done) {
	del('dist/build/build.js', done);
});

gulp.task('clean:css', function(done) {
	del('dist/build/build.css', done);
});

gulp.task('clean:images', function(done) {
	del('dist/images', done);
});

gulp.task('connect', ['build'], function() {
	connect.server({
		root: 'dist',
		livereload: true
	});
});

gulp.task('watch', function() {
	gulp.watch('src/**/*.twig', ['html']);
	gulp.watch('src/styles/**/*.scss', ['css']);
	gulp.watch('src/images/**/*', ['images']);
	gulp.watch([
		'src/scripts/**/*.js'
	], ['js']);
});

gulp.task('build', ['js', 'html', 'css', 'images']);

gulp.task('serve', ['connect', 'watch']);

gulp.task('default', ['build']);