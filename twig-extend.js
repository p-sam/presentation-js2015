module.exports = function(Twig) {
    Twig.filters.inline_code = function(code, params) {
        var e = Twig.filters.escape;
        var language = params.length ? params[0] : 'html';
        if(!String(code).trim()) {
            return '';
        }
        return '<code class="language-'+e(language, ['html_attr'])+'">'+e(code, ['html'])+'</code>';
    };
    Twig.filters.code = function(code, params) {
        if(!String(code).trim()) {
            return '';
        }
        return '<pre>'+Twig.filters.inline_code(code, params)+'</pre>';
    };
}