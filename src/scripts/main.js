// Require Node modules in the browser thanks to Browserify: http://browserify.org
var bespoke = require('bespoke');
var classes = require('bespoke-classes');
var keys = require('bespoke-keys');
var touch = require('bespoke-touch');
var scale = require('bespoke-scale');
var hash = require('bespoke-hash');
var progress = require('bespoke-progress');
var secondary = require('bespoke-secondary');
var indexfinger = require('bespoke-indexfinger');

// Bespoke.js
bespoke.from('article', [
	indexfinger(),
	classes(),
	keys(),
	touch(),
	scale(),
	hash(),
	progress(),
	secondary({
		keys: {
			toggle: 0x71 //F2,
		}
	})
]);

// Prism syntax highlighting
require('prismjs');